# README #

This README will explain how to install and run the test application.

### What is this repository for? ###

* Service Certainty Junior Full Stack Developer Test
* Version 1.0

### How do I get set up? ###

* To run this application, you will need a server that can run PHP code
* For this project I used WAMP Microsoft Windows server, however any server which can run phpMyAdmin should work.
* WAMP server can be downloaded at: https://sourceforge.net/projects/wampserver/files/. 
* You will first need to have Visual Studio 2012 installed, which can be found at: http://www.microsoft.com/en-us/download/details.aspx?id=30679
* Go through the set-up process until WAMP is installed on your machine. By default it is installed on 'C:\wamp64'.
* Start WAMP server by opening 'C:\wamp64\wampmanager.exe'.
* Download the repository.
* Extract the application and paste the folder with the application files into 'C:\wamp64\www'. e.g. 'C:\wamp64\www\seangrehan-test-758c09c580be'.
* Now go into phpMyAdmin by entering http://localhost/phpmyadmin/ into your web browser. The default user is 'root' with no password. 
* Now click on the databases tab to create a new database called 'testdb' with default collation. 
* Select 'testdb' and click on the import tab and click on choose file.
* Open 'C:\wamp64\www\seangrehan-test-758c09c580be\mysql_bak' and import 'testdb.sql'.
* This database has 1 table 'users'. 
* You can now open the application by going to 'http://localhost/seangrehan-test-758c09c580be/' in your web browser.

### Contact details ###

* seangrehan13@outlook.com