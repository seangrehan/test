<!DOCTYPE html>

<?php
echo file_get_contents("head.html");
echo file_get_contents("header.html");
if (isset($_COOKIE['user'])) {
  print '<li>
			<a href="userlist.php">User List</a>
		</li>
		<li>
			<a href="logout.php">Log-out</a>
		</li>
		';};
if (!isset($_COOKIE['user'])) {
	print '<li>
              <a href="login.php">Login</a>
          </li>
          <li>
              <a href="register.php">Register</a>
          </li>';};
  print '</ul></div></div></nav>';
?>
    
<div class="container">
    <div class="row">
        <div class="box">
            <div class="col-lg-12">
                <hr>
                <h2 class="intro-text text-center">
                <strong>Register for the test application</strong>
                </h2>
                <hr>
                    <form action="registerform.php" name="myform" id="myform" method="post">
                    <h3 class="intro-text text-center">Name: <input type="text" name="name" value='<?php if (isset($_COOKIE['name'])){$name = ($_COOKIE["name"]); print $name;} ?>'></h3>
                    <h3 class="intro-text text-center">Email: <input type="text" name="email" value='<?php if (isset($_COOKIE['email'])){$email = ($_COOKIE["email"]); print $email;} ?>'></h3>
                    <h3 class="intro-text text-center">Date of birth: <input type="date" name="dob" value='<?php if (isset($_COOKIE['dob'])){$dob = ($_COOKIE["dob"]); print $dob;} ?>'></h3>
                    <h3 class="intro-text text-center">Password: <input type="text" name="password"></h3>
					&nbsp;
					<h3 class="intro-text text-center">
					<?php if (isset($_COOKIE['name'])){$name = ($_COOKIE["name"]); print 'Incorrect Captcha,';} ?>
					Please type in the 3 green characters from the captcha</h3>
                    <h3 class="intro-text text-center"><img height="40px" width="auto" src="captcha.php" alt="captcha image"><input style="margin-left:20px;padding-left:40px;" type="text" name="captcha" size="3" maxlength="3"></h3>
					&nbsp;
                    <h3 class="intro-text text-center">
                    <input type="reset" name="reset" id="reset" value="Reset" class="intro-text text-center">
                    <input type="submit" name="submit" id="submit" value="Submit" class="intro-text text-center">
                    </h3>
                    &nbsp;
                    <h3 class="intro-text text-center">
                    <div id="myform_errorloc" class="p1"></div>
                    </h3>
                    </form>
                    <script language="JavaScript" type="text/javascript" xml:space="preserve">
                    var frmvalidator  = new Validator("myform");
                    frmvalidator.EnableOnPageErrorDisplaySingleBox();
                    frmvalidator.EnableMsgsTogether();
 
                    frmvalidator.addValidation("name","req","Please enter your name");
                    frmvalidator.addValidation("name","maxlen=255","Sorry your name cannot be longer than 255 characters");
					
                    frmvalidator.addValidation("email","req","Please enter your email");
                    frmvalidator.addValidation("email","email","Sorry, that is not a valid email address");
					frmvalidator.addValidation("email","maxlen=255","Sorry your email cannot be longer than 255 characters");

					frmvalidator.addValidation("dob","req","Please enter your date of birth");
					
					frmvalidator.addValidation("captcha","req","Please enter the captcha");
					
                    frmvalidator.addValidation("password","req","Please enter your password");
					frmvalidator.addValidation("password","minlen=5","Sorry your password cannot be shorter than 5 characters");
                    frmvalidator.addValidation("password","maxlen=255","Sorry your password cannot be longer than 255 characters");
                    </script>
            </div>
        </div>
    </div>
</div>

</body>
</html>
